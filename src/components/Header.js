import React from 'react';
import logo_anbu from '../res/logo_anbu_export.png';
const Header = () => {
    
    return (
        <div className="container-header">
            <div className="logo-header">
                <img src={logo_anbu} alt=""/>
            </div>
            <div className="title-header"> 
                <span>
                    ANBU
                </span>
                <span>
                    Anime
                </span>
            </div>
        </div>
    )
}

export default Header;
