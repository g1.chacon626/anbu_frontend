import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'normalize.css'
import Header from './components/Header';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
      <div className="App-header">
        <Header></Header>
      </div>
      {/* <div className="App-footer">
        <Footer></Footer>
      </div> */}
    </div>
  );
}

export default App;
